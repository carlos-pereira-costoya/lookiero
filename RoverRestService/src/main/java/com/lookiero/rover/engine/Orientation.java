package com.lookiero.rover.engine;

public enum Orientation {
	N, S, E, W
}
